# Twakeout

_Tools for interacting with Twitter archive/takeout files._

## Goal

For now:

* Be able to create a web page that contains links to _all_ of the tweets in the archive. The idea is to then point [Save Page Now](https://web.archive.org/save) to that page and have it archive everything. Not sure whether that’s gonna work at all for large datasets.

## Status

Work in progress.
**Not yet ready for general use, you need to be somewhat tech-savvy. It helps to be a Python programmer.**

That being said, currently it can:

* access tweets inside the ZIP file, i.e. without unpacking first (in fact it _doesn’t_ work with extracted data)
* gather really basic statistics
* print the permalinks to the tweets in the archive
* render arbitrary [Jinja](https://jinja.palletsprojects.com/) templates with your Tweets as input
* sort by creation date
* group by year/month/date
* work with large archives that contain `tweets-partN.js` files

## Installation

* Clone this repo.
* Run `poetry install`. (You need [Poetry](https://python-poetry.org/), of course.)
* Done.

If you don’t want to install Poetry _and_ know what you’re doing, you might get away with installing the dependencies listed in [`pyproject.toml`](pyproject.toml) manually.

## Running

```sh
# Get an overview of the available commands.
poetry run python -m twakeout --help

# Get help on a subcommand.
poetry run python -m twakeout render --help

# Create one HTML file per year, containing links to all of your tweets from that year.
poetry run python -m twakeout render --jinja example_templates/tweet_links.html --for-each year --output '{username}-{year}.tweets.html' --archive twitter-archive.zip
```

## Bugs

* The CLI interface isn’t well documented.
* In fact, this whole project isn’t well documented. I’m sorry, I’m in a bit of a hurry.
* We should probably provide an entrypoint script instead of relying on `python -m`.
* Sorting/grouping currently doesn’t work with large archives (i.e. those having `tweets-partN.js` files). This will be fixed ASAP.

## Contact

* [scy.name](https://scy.name/)
