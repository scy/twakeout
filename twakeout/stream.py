from enum import Enum
from itertools import groupby
import json
from typing import Any, BinaryIO, Callable, Dict, Iterable, Optional, Tuple

from .exceptions import ArchiveFormatError, ConflictingSettingsError, \
    JSFormatError
from .models import Tweet


class SortKey(Enum):
    NONE = 1
    CREATED = 2


class GroupBy(Enum):
    NONE = 1
    ID = 2
    YEAR = 3
    MONTH = 4
    DAY = 5


# Maps GroupBy values to key function suitable to use with `groupby`, and also
# a required pre-sorting that has to be used for the grouping to work.
TWEET_GROUP_SETTINGS: Dict[GroupBy, Tuple[SortKey, Callable[[Tweet], Any]]] = {
    GroupBy.NONE: (SortKey.NONE, lambda t: 0),
    GroupBy.ID: (SortKey.NONE, lambda t: t.id),
    GroupBy.YEAR: (SortKey.CREATED, lambda t: t.created_at.year),
    GroupBy.MONTH: (
        SortKey.CREATED, lambda t: (t.created_at.year, t.created_at.month)),
    GroupBy.DAY: (
        SortKey.CREATED, lambda t: (t.created_at.year, t.created_at.month,
                                    t.created_at.day)),
}


def js_remover(fp: BinaryIO) -> Iterable[bytes]:
    """Remove JavaScript boilerplate from an archive stream."""
    for raw_line in fp:
        line = raw_line.rstrip(b"\r\n")
        if line.startswith(b"window.") and line.endswith(b" = ["):
            continue
        if line == b"]":
            continue
        yield line


def json_obj_collector(lines: Iterable[bytes]) -> Iterable[dict]:
    """Collect lines into a JSON object."""
    json_data = b""
    for line in lines:
        if line == b"  {":  # New object.
            if len(json_data):
                raise JSFormatError("new object starts without old one ending")
            json_data = b"{\n"
        elif line == b"  }," or line == b"  }":  # Object ends.
            if not len(json_data):
                raise JSFormatError("object ends without any data")
            json_data += b"}\n"
            data = json.loads(json_data)
            json_data = b""
            if not isinstance(data, dict):
                raise JSFormatError("object is not a dict")
            yield data
        elif line.startswith(b"    "):
            json_data += line + b"\n"
        else:
            raise JSFormatError(f"unexpected line in object: {line}")


def read_js_tweet_stream(fp: BinaryIO) -> Iterable[Tweet]:
    for tweet_data in json_obj_collector(js_remover(fp)):
        if not isinstance(tweet_data.get("tweet", None), dict):
            raise ArchiveFormatError("did not find 'tweet' in stream")
        yield Tweet.parse_obj(tweet_data["tweet"])


def tweet_sorter(
    tweets: Iterable[Tweet],
    key: SortKey,
    reverse: bool
) -> Iterable[Tweet]:
    if key == SortKey.NONE:
        if reverse:
            tweet_list = list(tweets)
            tweet_list.reverse()
            yield from tweet_list
        yield from tweets
    elif key == SortKey.CREATED:
        yield from sorted(tweets, key=lambda t: t.created_at, reverse=reverse)


def tweet_grouper(
    tweets: Iterable[Tweet],
    by: GroupBy,
    *,
    sort: SortKey = SortKey.NONE,
    reverse: bool = False,
) -> Iterable[Iterable[Tweet]]:
    req_sort, key_func = TWEET_GROUP_SETTINGS[by]
    if sort == SortKey.NONE:
        # User has not requested sorting; use the one required by the grouping.
        sort = req_sort
    elif req_sort != SortKey.NONE and sort != req_sort:
        # User has requested a sort key conflicting with the grouping.
        raise ConflictingSettingsError(
            f"requested sort order '{sort.name.lower()}' conflicts with "
            f"requested grouping '{by.name.lower()}'")
    sorted_tweets = tweet_sorter(tweets, sort, reverse)
    for _, group in groupby(sorted_tweets, key_func):
        yield group


class StatsCollector:
    def __init__(self):
        self._count = 0
        self._oldest: Optional[Tweet] = None
        self._newest: Optional[Tweet] = None
        self._favs = 0
        self._rts = 0

    def __str__(self) -> str:
        return (
            f"{self._count} tweet{'' if self._count == 1 else 's'} "
            f"between {self._oldest.created_at} and {self._newest.created_at}"
            f"\n{self._favs} like{'' if self._favs == 1 else 's'}, "
            f"{self._rts} retweet{'' if self._rts == 1 else 's'}"
        )

    def track_tweet(self, tweet: Tweet):
        self._count += 1
        self._favs += tweet.favorite_count
        self._rts += tweet.retweet_count
        if self._oldest is None or tweet < self._oldest:
            self._oldest = tweet
        if self._newest is None or tweet > self._newest:
            self._newest = tweet
