class TwakeoutException(Exception):
    pass


class ArchiveFormatError(TwakeoutException):
    pass


class ConflictingSettingsError(TwakeoutException):
    pass


class JSFormatError(TwakeoutException):
    pass


class CLIError(TwakeoutException):
    pass
