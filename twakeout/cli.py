from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import Iterable

import jinja2

from .archive import ZIPArchive
from .exceptions import CLIError
from .models import Tweet
from .stream import GroupBy, SortKey, StatsCollector


def get_tweet_stream(args: Namespace) -> Iterable[Iterable[Tweet]]:
    if args.archive:
        tweets = ZIPArchive(args.archive[0])
    else:
        raise CLIError("no archive provided")
    return tweets.get_tweet_stream(
        sort=SortKey[args.sort_by.upper()],
        reverse=args.reverse,
        group_by=GroupBy[args.for_each.upper()],
    )


def wants_tweets(
    parser: ArgumentParser,
    sort: bool = True,
):
    parser.add_argument("--archive", "-a", nargs=1, type=Path)
    parser.set_defaults(
        sort_by=SortKey.NONE.name.lower(),
        reverse=False,
        for_each=GroupBy.NONE.name.lower(),
    )
    if sort:
        parser.add_argument("--sort-by", type=str, choices=[
            n.name.lower() for n in SortKey if n != SortKey.NONE])
        parser.add_argument("--reverse", action="store_true")


def render_cmd(args: Namespace):
    jenv = jinja2.Environment(
        loader=jinja2.FileSystemLoader("."),
    )
    jtpl = jenv.get_template(str(args.jinja))
    for group in get_tweet_stream(args):
        tweets = list(group)
        if not tweets:
            continue
        first = tweets[0]
        filename = args.output.format(
            year=first.created_at.year,
            month=first.created_at.month,
            day=first.created_at.day,
            username=first.account.username,
            id=first.id,
        )
        with open(filename, "x") as fp:
            fp.write(jtpl.render(
                tweets=tweets,
            ))


def stats_cmd(args: Namespace):
    for group in get_tweet_stream(args):
        stats = StatsCollector()
        for tweet in group:
            stats.track_tweet(tweet)
        print(stats)
        print()


def urls_cmd(args: Namespace):
    for group in get_tweet_stream(args):
        for tweet in group:
            print(tweet.url)


parser = ArgumentParser(
    prog="Twakeout",
)

subparsers = parser.add_subparsers(title="commands")

render_parser = subparsers.add_parser(
    "render",
    help="Render your tweets based on a template.",
)
render_parser.set_defaults(func=render_cmd)
wants_tweets(render_parser)
render_parser.add_argument(
    "--for-each", "-e",
    choices=[b.name.lower() for b in GroupBy if b != GroupBy.NONE],
    help="create a separate output file for each time period, or for each "
    "single tweet if set to 'id'",
)
render_parser.add_argument(
    "--jinja", "-j",
    action="store", metavar="PATH", required=True, type=Path,
    help="Jinja template to apply to your tweets",
)
render_parser.add_argument(
    "--output", "-o", metavar="NAME", required=True,
    help="output file name template",
)

stats_parser = subparsers.add_parser(
    "stats",
    help="Collect statistics about your tweets.",
)
stats_parser.set_defaults(func=stats_cmd)
wants_tweets(stats_parser, sort=False)
stats_parser.add_argument(
    "--for-each", "-e",
    choices=[
        b.name.lower() for b in GroupBy if b not in (GroupBy.NONE, GroupBy.ID)
    ],
    help="calculate separate stats for each time period",
)

urls_parser = subparsers.add_parser(
    "urls",
    help="Dump permalinks to all of your tweets.",
)
urls_parser.set_defaults(func=urls_cmd)
wants_tweets(urls_parser)


def run():
    args = parser.parse_args()
    args.func(args)
