from functools import lru_cache
from itertools import count
from pathlib import Path
from typing import Iterable
from zipfile import ZipFile

from .models import Account, Tweet
from .stream import GroupBy, SortKey, json_obj_collector, js_remover, \
    read_js_tweet_stream, tweet_grouper


class ZIPArchive:
    def __init__(self, path: Path):
        if not path.is_file:
            raise FileNotFoundError(str(path.absolute()))
        self._path = path

    def _get_tweet_filenames(self, zip: ZipFile) -> Iterable[str]:
        yield "data/tweets.js"
        for i in count(start=1):
            name = f"data/tweets-part{i}.js"
            try:
                zip.getinfo(name)
                yield name
            except KeyError:
                # Does not exist. We're done.
                return

    def get_tweet_stream(
        self,
        *,
        sort: SortKey = SortKey.NONE,
        reverse: bool = False,
        group_by: GroupBy = GroupBy.NONE,
    ) -> Iterable[Iterable[Tweet]]:
        with ZipFile(str(self._path), "r") as file:
            account = self.get_account()
            def account_assigner(t: Tweet) -> Tweet:
                t.account = account
                return t

            for filename in self._get_tweet_filenames(file):
                with file.open(filename) as tweets:
                    for group in tweet_grouper(
                        read_js_tweet_stream(tweets),
                        by=group_by, sort=sort, reverse=reverse,
                    ):
                        yield map(account_assigner, group)

    @lru_cache(maxsize=None)
    def get_account(self) -> Account:
        with ZipFile(str(self._path), "r") as file:
            with file.open("data/account.js") as account:
                return Account.parse_obj(
                    next(json_obj_collector(js_remover(account)))["account"])
