from __future__ import annotations
from datetime import datetime
from typing import Optional

from pydantic import BaseModel, Field, validator


class Account(BaseModel):
    id: str = Field(alias="accountId")
    username: str
    name: str = Field(alias="accountDisplayName")
    created_at: datetime = Field(alias="createdAt")


class Tweet(BaseModel):
    id: str
    account: Optional[Account]
    created_at: datetime
    favorite_count: int
    retweet_count: int
    full_text: str

    @validator("created_at", pre=True)
    def parse_date(cls, v: str) -> datetime:
        # The archive uses a custom datetime format.
        return datetime.strptime(v, "%a %b %d %H:%M:%S %z %Y")

    def __str__(self) -> str:
        account_info = f"@{self.account.username} " if self.account else ""
        return f"{account_info}[{self.id} {self.created_at}] {self.full_text}"

    def __gt__(self, other: Tweet):
        return self.created_at > other.created_at

    def __lt__(self, other: Tweet):
        return self.created_at < other.created_at

    @property
    def url(self) -> str:
        # Twitter actually ignores the username in the URL for permalinks, so
        # we can use basically anything as a fallback.
        user = self.account.username if self.account else "twitter"
        return f"https://twitter.com/{user}/status/{self.id}"
